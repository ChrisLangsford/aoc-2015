var fs = require('fs');
var data = fs.readFileSync('./input.txt', 'utf8').split('\r\n');

var totalPaper = data.map((present) => { return calculatePaperRequired(present) }).reduce((sum, val) => { return sum + val; });
console.log('total paper required: ' + totalPaper);

var totalRibbon = data.map((present) => { return calculateRibbonRequired(present) }).reduce((sum, val) => { return sum + val; });
console.log('total ribbon required: ' + totalRibbon);


function calculatePaperRequired(measurements) {
    let dims = measurements.split('x').map(Number);
    var dimensions = [dims[0] * dims[1], dims[1] * dims[2], dims[2] * dims[0]].sort((a, b) => { return a - b });
    return (2 * (dimensions[0] + dimensions[1] + dimensions[2])) + dimensions[0];
}
function calculateRibbonRequired(measurements) {
    let sortedLengths = measurements.split('x').map(Number).sort((a, b) => { return a - b });
    let sidesOfRibbon = 2 * (sortedLengths[0] + sortedLengths[1]);
    let bowRibbbon = sortedLengths[0] * sortedLengths[1] * sortedLengths[2];
    return sidesOfRibbon + bowRibbbon;
}