var fs = require('fs');
var instructions = fs.readFileSync('./input.txt', 'utf8');
var floor = 0;

// instructions.split('').map((a)=>{ (a=='(')? floor++: floor--;});
for (var i = 0; i < instructions.split('').length; i++) {
    if (instructions[i] == '(') {
        floor++;
    } else {
        floor--;
    }
    if (floor == -1) {
        console.log("basement"+ (i+1));        
    }
}
console.log(floor);
