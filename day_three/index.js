var fs = require('fs');
var data = fs.readFileSync('./input.txt', 'utf8').split('');
//deliver at current location
//  - find house at current location, if none create
//  - parse instruction and update current location
// repeat
var santa = santaFactory();
var robot = santaFactory();

var housesVisited = [houseFactory(santa.xPosition, santa.yPostition)];
var instructionCounter = 0;

data.forEach((instruction) => {
    if (instructionCounter % 2 === 0) {
        santa.move(instruction);
        console.log('Santa is at(' + santa.xPosition + ':' + santa.yPostition + ')');
        if (!housesVisited.find((house) => { return house.x === santa.xPosition && house.y === santa.yPostition })) {
            housesVisited.push(houseFactory(santa.xPosition, santa.yPostition));
        }
    } else {
        robot.move(instruction);
        console.log('Robot-Santa is at(' + robot.xPosition + ':' + robot.yPostition + ')');
        if (!housesVisited.find((house) => { return house.x === robot.xPosition && house.y === robot.yPostition })) {
            housesVisited.push(houseFactory(robot.xPosition, robot.yPostition));
        }

    }
    instructionCounter++;
})
console.log(housesVisited.length)

function houseFactory(x, y) {
    var house = {
        x: x,
        y: y
    };
    return house;
}

function santaFactory() {
    let santa = {
        xPosition: 0,
        yPostition: 0,
        move(instruction) {
            switch (instruction) {
                case '>':
                    this.xPosition++;
                    break;
                case '<':
                    this.xPosition--;
                    break;
                case '^':
                    this.yPostition++;
                    break;
                case 'v':
                    this.yPostition--;
                    break;
                default:
                    break;
            }
        }
    };
    return santa;
}
