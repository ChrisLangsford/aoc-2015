"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
exports.INPUT = fs.readFileSync("./input.txt", "utf8").split("\r\n");
var InstructionActions;
(function (InstructionActions) {
    InstructionActions["ON"] = "on";
    InstructionActions["OFF"] = "off";
    InstructionActions["TOGGLE"] = "toggle";
})(InstructionActions = exports.InstructionActions || (exports.InstructionActions = {}));
class Instruction {
    constructor(instructionString) {
        let stringPieces = instructionString.split(" ");
        let i = 0;
        if (stringPieces[i] === InstructionActions.TOGGLE) {
            this.action = InstructionActions.TOGGLE;
            i--;
        }
        else {
            this.action = this.convertStringtoAction(stringPieces[1]);
        }
        this.startX = +stringPieces[i + 2].split(",")[0];
        this.endX = +stringPieces[i + 2].split(",")[1];
        this.startY = +stringPieces[i + 4].split(",")[0];
        this.endY = +stringPieces[i + 4].split(",")[1];
    }
    convertStringtoAction(string) {
        return string === InstructionActions.ON
            ? InstructionActions.ON
            : InstructionActions.OFF;
    }
}
exports.Instruction = Instruction;
var LightState;
(function (LightState) {
    LightState["ON"] = "on";
    LightState["OFF"] = "off";
})(LightState = exports.LightState || (exports.LightState = {}));
class Light {
    constructor(state) {
        this.state = state;
    }
    toggle() {
        if (this.state == LightState.ON) {
            this.state = LightState.OFF;
        }
        else if (this.state == LightState.OFF) {
            this.state = LightState.ON;
        }
    }
}
exports.Light = Light;
class LightGrid {
    constructor() {
        this.lights = [];
        for (let i = 0; i < 1000; i++) {
            this.lights[i] = [];
            for (let j = 0; j < 1000; j++) {
                this.lights[i][j] = new Light(LightState.OFF);
            }
        }
    }
}
exports.LightGrid = LightGrid;
class Main {
    constructor() {
        this.instructionList = [];
        //create grid of switched off lights
        this.grid = new LightGrid();
    }
    run() {
        //convert input into list of objects that can be processed
        exports.INPUT.forEach((string) => {
            this.instructionList.push(new Instruction(string));
        });
        //process instructions
        // this.instructionList.forEach((instruction: Instruction)=>{
        // });
        this.processInstruction(new Instruction("turn on 0,0 through 999,0"));
        //count lights
        console.log("Lights Turned On: " + this.countOnLights(this.grid));
    }
    processInstruction(instruction) {
        for (let i = instruction.startX; i <= instruction.endX; i++) {
            for (let j = instruction.startY; i <= instruction.endY; i++) {
                switch (instruction.action) {
                    case InstructionActions.ON:
                        this.grid.lights[i][j].state = LightState.ON;
                    case InstructionActions.OFF:
                        this.grid.lights[i][j].state = LightState.OFF;
                    case InstructionActions.TOGGLE:
                        this.grid.lights[i][j].toggle();
                }
            }
        }
    }
    countOnLights(grid) {
        let count = 0;
        let flatArray = [].concat.apply([], grid.lights);
        flatArray.forEach((light) => {
            count += light.state === LightState.ON ? 1 : 0;
        });
        return count;
    }
}
exports.Main = Main;
new Main().run();
//# sourceMappingURL=index.js.map