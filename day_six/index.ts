import * as fs from "fs";

export const INPUT = fs.readFileSync("./input.txt", "utf8").split("\r\n");

export enum InstructionActions {
  ON = "on",
  OFF = "off",
  TOGGLE = "toggle"
}

export interface Instruction {
  action: InstructionActions;
  startX: number;
  endX: number;
  startY: number;
  endY: number;
}

export class Instruction implements Instruction {
  action: InstructionActions;
  startX: number;
  endX: number;
  startY: number;
  endY: number;

  constructor(instructionString: string) {
    let stringPieces = instructionString.split(" ");
    let i = 0;
    if (stringPieces[i] === InstructionActions.TOGGLE) {
      this.action = InstructionActions.TOGGLE;
      i--;
    } else {
      this.action = this.convertStringtoAction(stringPieces[1]);
    }

    this.startX = +stringPieces[i + 2].split(",")[0];
    this.startY = +stringPieces[i + 2].split(",")[1];
    this.endX = +stringPieces[i + 4].split(",")[0];
    this.endY = +stringPieces[i + 4].split(",")[1];
  }

  private convertStringtoAction(string) {
    return string === InstructionActions.ON
      ? InstructionActions.ON
      : InstructionActions.OFF;
  }
}

export enum LightState {
  ON = "on",
  OFF = "off"
}
export class Light {
  state: LightState;
  brightness: number;

  constructor(state: LightState) {
    this.state = state;
    this.brightness = 0;
  }

  brightnessOn(){
    this.brightness+=1;
  }
  brightnessOff(){
    if(this.brightness > 0){
      this.brightness-=1;
    }    
  }
  brightnessToggle(){
    this.brightness+=2;
  }

  switchOn() {
    this.state = LightState.ON;
  }

  switchOff() {
    this.state = LightState.OFF;
  }

  toggle() {
    if (this.state == LightState.ON) {
      this.switchOff();
    } else if (this.state == LightState.OFF) {
      this.switchOn();
    }
  }
}

export class LightGrid {
  lights: Light[][];

  constructor() {
    this.lights = [];
    for (let i = 0; i < 1000; i++) {
      this.lights[i] = [];
      for (let j = 0; j < 1000; j++) {
        this.lights[i][j] = new Light(LightState.OFF);
      }
    }
  }
}

export class PartA {
  private instructionList: Instruction[] = [];

  //create grid of switched off lights
  grid: LightGrid = new LightGrid();

  constructor() {}

  run(input: string[]): string {
    //convert input into list of objects that can be processed
    input.forEach((string: string) => {
      this.instructionList.push(new Instruction(string));
    });

    //process instructions
    this.instructionList.forEach((instruction: Instruction) => {
      //console.log(JSON.stringify(instruction));
      this.processInstruction(instruction);
    });

    //count lights
    let s: string = "Lights Turned On: " + this.countSwitchedOnLights();
    console.log(s);
    return s;
  }

  processInstruction(instruction: Instruction) {
    for (let i = instruction.startY; i <= instruction.endY; i++) {
      for (let j = instruction.startX; j <= instruction.endX; j++) {
        if (instruction.action == InstructionActions.TOGGLE)
          this.grid.lights[i][j].toggle();
        if (instruction.action == InstructionActions.ON)
          this.grid.lights[i][j].switchOn();
        if (instruction.action == InstructionActions.OFF)
          this.grid.lights[i][j].switchOff();
      }
    }
  }

  countSwitchedOnLights(): number {
    let count: number = 0;
    let flatArray: Light[] = [].concat.apply([], this.grid.lights);
    flatArray.forEach((light: Light) => {
      count += light.state === LightState.ON ? 1 : 0;
    });
    return count;
  }
}

export class PartB {
  private instructionList: Instruction[] = [];

  //create grid of switched off lights
  grid: LightGrid = new LightGrid();

  constructor() {}

  run(input: string[]): string {
    //convert input into list of objects that can be processed
    input.forEach((string: string) => {
      this.instructionList.push(new Instruction(string));
    });

    //process instructions
    this.instructionList.forEach((instruction: Instruction) => {
      //console.log(JSON.stringify(instruction));
      this.processInstruction(instruction);
    });

    //count lights
    let s = "Total Brightness: " + this.calculateBrightness();
    console.log(s);
    return s;
  }

  processInstruction(instruction: Instruction) {
    for (let i = instruction.startY; i <= instruction.endY; i++) {
      for (let j = instruction.startX; j <= instruction.endX; j++) {
        if (instruction.action == InstructionActions.TOGGLE)
          this.grid.lights[i][j].brightnessToggle();
        if (instruction.action == InstructionActions.ON)
          this.grid.lights[i][j].brightnessOn();
        if (instruction.action == InstructionActions.OFF)
          this.grid.lights[i][j].brightnessOff();
      }
    }
  }

  calculateBrightness(): number {
    let totalBrightness: number = 0;
    let flatArray: Light[] = [].concat.apply([], this.grid.lights);
    flatArray.forEach((light: Light) => {
      totalBrightness += light.brightness;
    });
    return totalBrightness;
  }
}

new PartA().run(INPUT);
new PartB().run(INPUT);
