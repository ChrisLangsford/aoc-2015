"use strict";
exports.__esModule = true;
var index_1 = require("./../index");
require("mocha");
var chai_1 = require("chai");
describe("Array", function () {
    describe("#indexOf()", function () {
        it("should return -1 when the value is not present", function () {
            chai_1.expect(1).to.equal(1);
        });
    });
});
describe("AoC Basic Tests", function () {
    it("Should turn on every light", function () {
        var input = "turn on 0,0 through 999,999".split("\r\n");
        chai_1.expect(new index_1.Main().run(input)).to.equal("Lights Turned On: " + 1000000);
    });
    it("Should toggle on the first row of lights", function () {
        var input = "toggle 0,0 through 999,0".split("\r\n");
        chai_1.expect(new index_1.Main().run(input)).to.equal("Lights Turned On: " + 1000);
    });
    it("Should turn on the 4 middle lights", function () {
        var input = "turn on 499,499 through 500,500".split("\r\n");
        chai_1.expect(new index_1.Main().run(input)).to.equal("Lights Turned On: " + 4);
    });
});
