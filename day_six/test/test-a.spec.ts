import { PartA, PartB } from "../index";
import "mocha";
import { expect } from "chai";

describe("Array", function() {
  describe("#indexOf()", function() {
    it("should return -1 when the value is not present", function() {
      expect(1).to.equal(1);
    });
  });
});

describe("Part A Basic Tests", () => {
  it("Should turn on every light", () => {
    let input: string[] = "turn on 0,0 through 999,999".split("\r\n");
    expect(new PartA().run(input)).to.equal("Lights Turned On: " + 1000000);
  });
  it("Should toggle on the first row of lights", () => {
    let input: string[] = "toggle 0,0 through 999,0".split("\r\n");
    expect(new PartA().run(input)).to.equal("Lights Turned On: " + 1000);
  });
  it("Should turn on the 4 middle lights", () => {
    let input: string[] = "turn on 499,499 through 500,500".split("\r\n");
    expect(new PartA().run(input)).to.equal("Lights Turned On: " + 4);
  });
});

describe("Part B Basic Tests", () => {
  it("Should increase brightness by 1", () => {
    let input: string[] = "turn on 0,0 through 0,0".split("\r\n");
    expect(new PartB().run(input)).to.equal("Total Brightness: "+ 1);
  });
  it("Should increase brightness by 2000000", () => {
    let input: string[] = "toggle 0,0 through 999,999".split("\r\n");
    expect(new PartB().run(input)).to.equal("Total Brightness: "+ 2000000);
  });
});
