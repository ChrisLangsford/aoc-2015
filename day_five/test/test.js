var assert = require("assert");
var indexModule = require("../index");
let vowelRegex = /[aeiou]/gi;
let part2Regex = /[(.)]/gi;

describe("Array", function() {
  describe("#indexOf()", function() {
    it("should return -1 when the value is not present", function() {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});

describe("nice strings", () => {
  it("should return false for forbidden strings", () => {
    assert.equal(
      false,
      indexModule.forbiddenStringChecker.containsForbiddenString(
        "ugknbfddgicrmopn"
      )
    );
  });

  it("should return true where there are double letters", () => {
    assert.equal(true, indexModule.doubleLetterChecker("aabcd"));
  });

  it("should have 3 or more vowels", () => {
    assert.equal(true, indexModule.stringHasNiceNumberOfVowels("abcdei", vowelRegex, 3));
  });
});

describe("naughty strings", () => {
  it("should return true for forbidden strings", () => {
    assert.equal(
      true,
      indexModule.forbiddenStringChecker.containsForbiddenString(
        "haegwjzuvuyypxyu"
      )
    );
  });

  it("should return false where there are no double letters", () => {
    assert.equal(false, indexModule.doubleLetterChecker("abcd"));
  });

  it("should have 3 or more vowels", () => {
    assert.equal(false, indexModule.stringHasNiceNumberOfVowels("abcd", vowelRegex, 3));
  });
});

describe("operations", () => {
  it("filters out strings with forbidden substrings", () => {
    let input = ["ugknbfddgicrmopn", "haegwjzuvuyypxyu"];
    let expectedOutput = ["ugknbfddgicrmopn"];
    assert.deepEqual(
      expectedOutput,
      indexModule.main.stripForbiddenStrings(input)
    );
  });

  it("should strip non double letter strings from the list", () => {
    let input = ["aabcd", "abcd", "abba"];
    let expectedOutput = ["aabcd", "abba"];
    assert.deepEqual(
      expectedOutput,
      indexModule.main.stripNoDoubleLetters(input)
    );
  });

  it("should strip strings without 3 or more vowels", () => {
    let input = ["aabcde", "abcd", "abba"];
    let expectedOutput = ["aabcde"];
    assert.deepEqual(
      expectedOutput,
      indexModule.main.stripNaughtyVowelStrings(input, vowelRegex, 3)
    );
  });
});

describe("part 2", () => {
  describe("test 2 letters twice without overlap", ()=>{
    it("should return true for aabcdefgaa", ()=>{
      assert.equal(true, indexModule.stringHasNiceNumberOfVowels("aabcdefgaa", part2Regex, 2));
    });
  });
});
