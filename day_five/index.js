var fs = require('fs');
var INPUT = fs.readFileSync('./input.txt', 'utf8').split('\r\n');

var exports = module.exports = {};

exports.forbiddenStringChecker = {
    forbiddenStrings: ["ab", "cd", "pq", "xy"],
    containsForbiddenString: function(stringToCheck){
        return (new RegExp(this.forbiddenStrings.join("|")).test(stringToCheck))
       
    }
};

exports.doubleLetterChecker = function(string){
    return new RegExp(/(.)\1/i).test(string);
}

exports.stringHasNiceNumberOfVowels = function(string, regex, count){
    var m = string.match(regex);
    return m === null ? 0 : m.length >= count;
}



exports.main = {
    stripForbiddenStrings: function(strings){
        return strings.filter((string)=>{
            return !exports.forbiddenStringChecker.containsForbiddenString(string);
        });
    },
    stripNoDoubleLetters: function(strings){
        return strings.filter((string)=>{
            return exports.doubleLetterChecker(string);
        });
    },
    stripNaughtyVowelStrings: function(strings, regex, count){
        return strings.filter((string)=>{
            return exports.stringHasNiceNumberOfVowels(string, regex, count);
        });
    },
    part1: function(){
        let vowelRegex = /[aeiou]/gi;
        let niceList = this.stripNaughtyVowelStrings(this.stripNoDoubleLetters(this.stripForbiddenStrings(INPUT)),vowelRegex, 3);
        console.log("Part 1: "+ niceList.length);
    },
    part2: function(){
        let twoLetterRegex = /[.]/gi;
        let niceList = this.stripNaughtyVowelStrings(INPUT, twoLetterRegex, 2);
        console.log("Part 2: "+ niceList.length);
    }
};

exports.main.part1();
//exports.main.part2();

const isContainPair = string => /([a-z][a-z]).*\1/.test(string);
const isContainRepeatLetter = string => /([a-z])[a-z]\1/.test(string);

const isNiceString = string => !!(isContainPair(string) && isContainRepeatLetter(string));

const result = INPUT.reduce((total, string) => isNiceString(string) ? ++total : total, 0);

console.log("Part 2: "+ result);

